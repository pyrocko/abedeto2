
from pyrocko.guts import Object, String, List
from pyrocko.squirrel import CodesNSLCE

from pyrocko.has_paths import HasPaths, Path


class SeismicArray(Object):
    pass


class SeismicArrayFromFDSN(SeismicArray):
    name = String.T()
    site = String.T()
    codes = List.T(CodesNSLCE.T())


seismic_arrays = [
    SeismicArrayFromFDSN(
        name=name,
        site='iris',
        codes=[CodesNSLCE(c) for c in codes])
    for (name, codes) in [
        ('YKA', ['CN.YKA*..SHZ']),
        # ('ESK', ['IM.EKB?..SHZ',
        #        'IM.EKR*..SHZ']),
        # ('ESK1', 'IM.EKA?..SHZ'),
        ('ILAR', ['IM.IL*..SHZ']),
        ('IMAR', ['IM.IM0?..SHZ']),
        # 'NIA', 'IM.I56H?..SHZ',
        # 'PFIA', ['IM.I57H?..SHZ',
        #         'IM.I57L?..SHZ'],
        ('BMA', ['IM.BM0?..SHZ']),
        ('BCA', ['IM.BC0?..SHZ']),
        # 'HIA', 'IM.I59H?..SHZ',
        ('NVAR', ['IM.NV*..SHZ']),
        ('PDAR', ['IM.PD0*..SHZ', 'IM.PD1*..SHZ']),
        ('TXAR', ['IM.TX*..SHZ']),
        # 'Pilbara', 'AU.PSA*..SHZ',
        ('AliceSprings', ['AU.AS*..SHZ']),
        # 'GERES', ['IM.GEA?..SHZ',
        #         'IM.GEB?..SHZ',
        #         'IM.GEC?..SHZ',
        #         'IM.GED?..SHZ'],
        # Diego Garcia Hydroacoustic array noqa
        ('DGHAland', ['IM.I52H?..SHZ']),
        ('DGHAS', ['IM.H08S?..SHZ']),
        ('DGHAN', ['IM.H08N?..SHZ']),
        # Tristan da Cunha. 'SHZ', BDF. noqa
        # 'TDC', ['IM.H09N?..SHZ',
        #        'IM.I49H?..SHZ'],
        # 'NarroginIA', 'IM.I04H?..SHZ',
        # 'CocosIslands', 'IM.I06H?..SHZ',
        ('Warramunga', ['IM.I07H?..SHZ'])
        # 'BermudaIA', 'IM.I51H?..SHZ',
        # 'FairbanksIA', 'IM.I53H?..SHZ'
    ]
] + [
    SeismicArrayFromFDSN(
        name=name,
        site='geofon',
        codes=[CodesNSLCE(c) for c in codes])
    for (name, codes) in [
        ('ROHRBACH', ['6A.V*..SHZ']),
        ('AntaOffshore', ['GR.I27L?.*.SHZ']),
        ('AntaOnshore', ['AW.VNA*.*.SHZ']),
    ]
] + [
    SeismicArrayFromFDSN(
        name=name,
        site='bgr',
        codes=[CodesNSLCE(c) for c in codes])
    for (name, codes) in [
        ('GERES', [
            'GR.GEA?.*.SHZ',
            'GR.GEB?.*.SHZ',
            'GR.GEC?.*.SHZ',
            'GR.GED?.*.SHZ']),
    ]
]


class SeismicArrayFromFile(SeismicArray, HasPaths):
    name = String.T()
    stations_path = Path.T()

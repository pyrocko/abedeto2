import os
import logging
from pyrocko import model
from . import arrays


logger = logging.getLogger('abedeto.core')


class Abedeto:

    def __init__(self, config):

        self._config = config
        self._events = model.load_events(
            config.expand_path(config.events_path))

        self._arrays = arrays.seismic_arrays + config.custom_arrays

    def get_abedeto_dir(self):
        return self._config.expand_path(self._config.abedeto_path)

    def get_array_names(self):
        return [arr.name for arr in self._arrays]

    def get_event_names(self):
        return [ev.name for ev in self._events]

    def __str__(self):
        return '''
Events:
%s
Arrays:
%s
''' % (
            '\n'.join('  %s' % name for name in self.get_event_names()),
            '\n'.join('  %s' % name for name in self.get_array_names()))

    def iter_event_array(self, event_names=None, array_names=None):
        if not event_names:
            event_names = self.get_event_names()
        if not array_names:
            array_names = self.get_array_names()

        for event in self._events:
            if event.name in event_names:
                for array in self._arrays:
                    if array.name in array_names:
                        yield event, array

    def get_store_id(self, event, array):
        return self._config.store_id_template.format(
            event=event.name, array=array.name)

    def get_store_path(self, store_id):
        return os.path.join(self.get_abedeto_dir(), 'gf_stores', store_id)

    def get_store(self, event, array):
        store_id = self.get_store_id(event, array)
        path = self.get_store_path(store_id)
        if not os.path.exists(path):
            # propose and create store, calculate ttt

        else:
            store = # open store

        return store

    def download(self, event_names=None, array_names=None):

        for event, array in self.iter_event_array(event_names, array_names):
            logger.info(
                'Downloading event %s, array %s' % (event.name, array.name))

            store = self.get_store(event, array)

            tmin = store.t(self._config.tmin, event, station)
            tmax = store.t(self._config.tmax, event, station)


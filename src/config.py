
import os.path as op
from pyrocko.guts import List, Bool, Float, StringChoice, String
from pyrocko import guts, gf
from pyrocko.has_paths import HasPaths, Path
from .error import AbedetoError
from .arrays import SeismicArray

guts_prefix = 'abedeto'


class Quantity(StringChoice):
    choices = ['raw', 'velocity']


class Config(HasPaths):
    events_path = Path.T()
    custom_arrays = List.T(SeismicArray.T())
    quantity = Quantity.T()
    fmin = Float.T()
    fmax = Float.T()
    depth_min = Float.T()
    depth_max = Float.T()
    depth_delta = Float.T()
    normalize = Bool.T()
    tmin = gf.Timing.T()
    tmax = gf.Timing.T()
    store_id_template = String.T(default='gf_{event}_{array}')
    abedeto_path = Path.T(default='abedeto')


def load(path):
    config = guts.load(filename=path)
    if not isinstance(config, Config):
        raise AbedetoError('Invalid Abedeto configuration: %s' % path)
    config.set_basepath(op.dirname(path) or '.')
    return config


def dump(config, path):
    basepath = config.get_basepath()
    dirname = op.dirname(path) or '.'
    config.change_basepath(dirname)
    guts.dump(config, filename=path)
    config.change_basepath(basepath)

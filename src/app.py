from pyrocko import squirrel, gf
from . import config, core


def lsplit(s):
    if s is None:
        return None
    return [v.strip() for v in s.split(',')]


class AbedetoInit(squirrel.SquirrelCommand):
    def make_subparser(self, subparsers):
        return subparsers.add_parser(
            'init', help='Get initial configuration.')

    def setup(self, parser):
        pass

    def run(self, parser, args):
        conf = config.Config(
            events_path='events.yaml',
            quantity='velocity',
            fmin=0.5,
            fmax=2.0,
            depth_min=1000.,
            depth_max=20000.,
            depth_delta=1000.,
            normalize=False,
            tmin=gf.Timing('{stored:P}-10'),
            tmax=gf.Timing('{stored:P}+52'))

        print(conf)


class AbedetoInfo(squirrel.SquirrelCommand):
    def make_subparser(self, subparsers):
        return subparsers.add_parser(
            'info', help='Show available arrays and events.')

    def setup(self, parser):
        parser.add_argument(
            'config_path',
            help='Path to configuration file.')

    def run(self, parser, args):
        conf = config.load(args.config_path)
        abedeto = core.Abedeto(conf)
        print(abedeto)


class AbedetoDownload(squirrel.SquirrelCommand):
    def make_subparser(self, subparsers):
        return subparsers.add_parser(
            'download', help='Downlad seismic waveforms.')

    def setup(self, parser):
        parser.add_argument(
            'config_path',
            help='Path to configuration file.')
        parser.add_argument(
            '--events',
            dest='event_names',
            metavar='EVENT,...',
            help='Names of events to download.')
        parser.add_argument(
            '--arrays',
            dest='array_names',
            metavar='ARRAY,...',
            help='Names of arrays to download.')

    def run(self, parser, args):
        conf = config.load(args.config_path)
        abedeto = core.Abedeto(conf)

        event_names = lsplit(args.event_names)
        array_names = lsplit(args.array_names)

        abedeto.download(event_names, array_names)


def main():
    squirrel.run(
        subcommands=[AbedetoInit(), AbedetoInfo(), AbedetoDownload()],
        description='What was the depth, again?')

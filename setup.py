
from setuptools import setup

setup(
    name='abedeto',
    version='2.0',
    author='The Abedeto Developers',
    maintainer='Sebastian Heimann',
    maintainer_email='sebastian.heimann@uni-potsdam.de',
    license='AGPLv3',
    package_dir={
        'abedeto': 'src'
    },
    packages=[
        'abedeto',
    ],
    install_requires=[
        'pyrocko>2022.11.29',
    ],
    entry_points={
        'console_scripts': ['abedeto = abedeto.app:main']
    }
)
